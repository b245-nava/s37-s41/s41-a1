const mongoose = require('mongoose');
const User = require('../Models/userSchema.js');
const Course = require('../Models/coursesSchema.js')
const bcrypt = require('bcrypt');
const auth = require('../auth.js');


// Controllers


// This controller will create/register a user in our database.
module.exports.userRegistration = (request, response) => {

	const input = request.body;

	User.findOne({email: input.email})
	.then(result => {

		if(result !== null){
			return response.send("A user with this email already exists!");
		} else {
			let newUser = new User({
				firstName: input.firstName,
				lastName: input.lastName,
				email: input.email,
				password: bcrypt.hashSync(input.password, 10),
				mobileNo: input.mobileNo
			})

			newUser.save()
			.then(save => {
				return response.send("You are now registered. Thank you!");
			})
			.catch(error => {
				return response.send(error);
			});
			
		}
	})
	.catch(error => {
		return response.send(error)
	});
};

// User Authentication
module.exports.userAuthentication = (request, response) => {

	let input = request.body;

	/*
		Possible scenarios:
			1. Email is not yet registered
			2. Email is registered, but the password is wrong
	*/

	User.findOne({email: input.email})
	.then(result => {

		// Email registration check
		if(result === null){
			return response.send("Email is not yet registered. Please register before logging in.");
		} else {

			// Password verification
			// .CompareSync() is used to compare a non-encrypted password to the encrypted password. It returns a boolean value: true if they match, false if not.

			const isPasswordCorrect = bcrypt.compareSync(input.password,result.password);

			if(isPasswordCorrect){
				return response.send({auth: auth.createAccessToken(result)});
			} else {
				return response.send("Password is incorrect.");
			}

		}
	})
	.catch(error => {
		return response.send(error);
	})

};

// getUser Controller
module.exports.getUser = (request, response) => {

	// const input = request.body;
	const userData = auth.decode(request.headers.authorization);
	// console.log(userData);

	return User.findById(userData._id)
	.then(result => {
		result.password = "";
		return response.send(result);
	})
};

/*
	Controller for User Enrollment

		1. We can get the ID of the user by decoding the JWT
		2. We can get the course ID by using the request params
*/

module.exports.enrollCourse = async(request, response) => {

	// First, we have to get the userId and the courseId.
	// We need to decode the token to extract/unpack the payload.

	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;

	// We now have the user's info decoded and the course's ID contained in variables.
	// Again, the two things we need to achieve with this controller:
		// 1. Push the courseID into the user's course enrollments property.
		// 2. Push the userID into the course's list of enrollees property.

	let isUserUpdated = await User.findById(userData._id)
		.then(result => {

			if(result === null){
				return false
			} else if (result.isAdmin === true){
				return false
			} else {
				Course.findById(courseId)
				.then(result => {

					if(result === null){
						return false
					} else {
						result.enrollments.push({courseId: courseId})

						return result.save()
						.then(save => true)
						.catch(err => false)
					}
				})
			}
		})
		.catch(err => response.send(err));

	let isCourseUpdated = await Course.findById(courseId)
		.then(result => {

			if(result === null){
				return false
			} else {

				result.enrollees.push({userId: userData._id})

				return result.save()
				.then(save => true)
				.catch(err => false)
			}
		})
		.catch(err => response.send(err));

	/*console.log(isUserUpdated);
	console.log(isCourseUpdated);*/

	if(isUserUpdated && isCourseUpdated){
		return response.send("Congratulations! You have been enrolled in this course.")
	} else {
		return response.send("There was an error during the enrollment. Either you aren't allowed to enroll, or there is something wrong with your user ID or the course ID. Please log in to a valid user account, check your desired course ID, and try again!")
	}

};