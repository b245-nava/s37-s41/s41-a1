const mongoose = require('mongoose');
const Course = require('../Models/coursesSchema.js');
const jwt = require('jsonwebtoken');
const auth = require('../auth.js');

// Create a new course
/*
	Steps:
		1. Create a new Course object using the mongoose model and the information from the equest body and the id from the header.
		2. Save the new User to the database.
*/

module.exports.addCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	console.log(userData);

	if(userData.isAdmin == true){
		const input = request.body;

		let newCourse = new Course({
			name: input.name,
			description: input.description,
			price: input.price
		});

		// Save function
		return newCourse.save()
		.then(course => {
			console.log(course)
			response.send(`The course ${course.name} has been created.`);
		})
		.catch(error => {
			console.log(error);
			response.send(false);
		})
	} else {
		return response.send(`You are not authorized to create a course.`)
	}
	
};

// Create a controller where it will retrieve all the courses (both active and inactive)

module.exports.allCourses = (request, response) => {
	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){
		return response.send('You are not authorized to view this.')
	} else {
		Course.find({})
		.then(result => response.send(result))
		.catch(error => response.send(error));
	}
};

// Create a controller for retrieving courses that are active

module.exports.allActiveCourses = (request, response) => {

	Course.find({isActive: true})
	.then(result => response.send(result))
	.catch(error => response.send(error));
};

/*
	Mini-Activity:
		1. Create a route where it can retrieve all inactive courses.
		2. Make sure the admin users only can access this route.
*/

module.exports.allInactiveCourses = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(!userData.isAdmin){
		return response.send('You are not authorized to view this.')
	} else {
		Course.find({isActive: false})
		.then(result => response.send(result))
		.catch(error => response.send(error))
	}
};

// Controller for details of a specific course

module.exports.courseDetails = (request, response) => {

	// To retrieve params from the URL
	const courseId = request.params.courseId;

	Course.findById(courseId)
	.then(result => response.send(result))
	.catch(err => response.send(err));
};

// Controller for updating specific course
/*
	Business Logic:
		1. We're going to edit/update the course indicated in the params
		2. User needs to be verified as admin
*/
module.exports.updateCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;

	const input = request.body;

	if(!userData.isAdmin){
		return response.send('You are not authorized to do this! If you think that this is a mistake, please contact your administrator.')
	} else {

		// If one our of the possible total 24 digits is incorrect...
		Course.findOne({_id: courseId})
		.then(result => {

			if(result === null){
				return response.send('CourseId is invalid. Please double-check the ID and try again.')
			} else {
				let updatedCourse = {
					name: input.name,
					description: input.description,
					price: input.price
				}

				
				Course.findByIdAndUpdate(courseId, updatedCourse, {new: true})
				.then(result => {
					console.log(result)
					return response.send(result)})
				.catch(err => response.send(err))
			}
		})
		.catch(err => response.send(error))
	}
};

// Controller for archiving a course

module.exports.archiveCourse = (request, response) =>{

	const userData = auth.decode(request.headers.authorization);
	const courseId = request.params.courseId;
	const input = request.body

	if(!userData.isAdmin){
		return response.send('You are not authorized to do this. Please contact your administrator if you think this is a mistake.')
	} else {

		Course.findById({_id: courseId})
		.then(result => {

			if(result === null){
				return response.send('The given course ID is incorrect. Please double-check and try again.')
			} else {

				let updatedStatus = {
					isActive: input.isActive
				}

				Course.findByIdAndUpdate(courseId, updatedStatus, {new:true})
				.then(result => response.send(result))
				.catch(err => response.send(err))
			}
		})
		.catch(err => response.send(error))
	}
};